from qgis.core import QgsProviderMetadata

class MeshDataProviderMetadata(QgsProviderMetadata):
    """base class for mesh data providers, please note that this class
    is called in a multithreaded context"""

    def __init(self, key, description, createFunc):
    	""""""
    	super().__init(key, description, createFunc)

    def decodeUri(self,uri):
    	# return dict([element.split('=') for element in uri.split(' ') if len(element.split('='))==2])
    	return {}