from builtins import object
# -*- coding: UTF-8 -*-

class Plugin(object):
    """Method to load the plugin"""
    def __init__(self, iface):
        """Constructor

        :param iface: qgis interface
        :type iface:QgisInterface
        """
        pass

    def initGui(self):
        pass

    def unload(self):
        pass

def classFactory(iface):
    """Method to be recognised by QGIS as a plugin"""
    return Plugin(iface)

